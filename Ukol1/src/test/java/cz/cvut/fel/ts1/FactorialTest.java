package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {

    Factorial FactorialCalculator=  new Factorial();
    @Test
    void testFactorialZero() {
        assertEquals(1, FactorialCalculator.calculateFactorial(0));
    }

    @Test
    void testFactorialOne() {
        assertEquals(1, FactorialCalculator.calculateFactorial(1));
    }

    @Test
    void testFactorialTwo() {
        assertEquals(2, FactorialCalculator.calculateFactorial(2));
    }

    @Test
    void testFactorialThree() {
        assertEquals(6, FactorialCalculator.calculateFactorial(3));
    }

    @Test
    void testFactorialFour() {
        assertEquals(24, FactorialCalculator.calculateFactorial(4));
    }

    @Test
    void testFactorialFive() {
        assertEquals(120, FactorialCalculator.calculateFactorial(5));
    }

    @Test
    void testFactorialNegative() {
        assertThrows(IllegalArgumentException.class, () -> {
            FactorialCalculator.calculateFactorial(-1);
        });
    }
}