package cz.cvut.fel.ts1;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number to calculate its factorial: ");
        int number = scanner.nextInt();

        Factorial factorial = new Factorial();
        long res= factorial.calculateFactorial(number);
        System.out.println("Factorial of " + number + " is: " + res);
        scanner.close();
    }


}
